﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicamentosWebAPI.Models
{
    public class Medicamento
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public int FarmacoId { get; set; } //propriedade chave estrangeira
       public Farmaco Farmaco { get; set; } //propriedade navegação
    }
}
