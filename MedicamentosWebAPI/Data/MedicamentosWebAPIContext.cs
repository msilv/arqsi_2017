﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MedicamentosWebAPI.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace MedicamentosWebAPI.Models
{
    public class MedicamentosWebAPIContext : IdentityDbContext<UserEntity>
    {
        public MedicamentosWebAPIContext (DbContextOptions<MedicamentosWebAPIContext> options)
            : base(options)
        {
        }

        public DbSet<MedicamentosWebAPI.Models.Medicamento> Medicamento { get; set; }

        public DbSet<MedicamentosWebAPI.Models.Farmaco> Farmaco { get; set; }
    }
}
